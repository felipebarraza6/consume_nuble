# Python
import requests

# DRF
from rest_framework import mixins, viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

# Permissions
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated
)

# Serializers
from core.users.serializers import UserModelSerializer, UserSignUpSerializer

# Models
from core.users.models import User
from oauth2_provider.models import AccessToken

# Filters
from django_filters import rest_framework as filters

CLIENT_ID = 'I9av8huL4ej0Yv955exr9mymxicHoZoBcsatYOzk'
CLIENT_SECRET = '6JSBBUWUx0LAQ48C5dTtdiByMzSSJD26TZ1Le5BtiXFhmzNRCa6slUg6b3LnJexCbX2gbgFWTdCvmF153Rfkgb5ECWv2X6oXgtM0DLVdjTDusCz06fS25y5N1hQWS83I'

# User View Set
class UserViewSet(viewsets.GenericViewSet,
                  mixins.RetrieveModelMixin, 
                  mixins.UpdateModelMixin, 
                  mixins.ListModelMixin,
                  mixins.DestroyModelMixin):

    def get_permissions(self):
        """Assign permissions based on action."""
        if self.action in ['register', 'token', 'refresh_token', 'revoke_token']:
            permissions = [AllowAny]
        elif self.action in ['retrieve', 'list', 'delete', 'patch']:
            permissions = [IsAuthenticated]
        else:
            permissions = [IsAuthenticated]
        return [p() for p in permissions]

    queryset = User.objects.all()
    serializer_class = UserModelSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    lookup_field = 'username'
    
    @action(detail=False, methods=['post'])
    def register(self, request):
        serializer = UserSignUpSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()

            r = requests.post('http://0.0.0.0:8000/oauth/token/',
                data={
                    'grant_type': 'password',
                    'username': request.data['email'],
                    'password': request.data['password'],
                    'client_id': CLIENT_ID,
                    'client_secret': CLIENT_SECRET
                }
            )

            return Response(r.json())
        return Response(serializer.errors)

    @action(detail=False, methods=['post'])
    def token(self, request):
        r = requests.post('http://0.0.0.0:8000/oauth/token/',
            data={
                'grant_type': 'password',
                'username': request.data['username'],
                'password': request.data['password'],
                'client_id': CLIENT_ID,
                'client_secret': CLIENT_SECRET
            }
        )
       
        return Response(r.json())

    @action(detail=False, methods=['post'])
    def refresh_token(self, request): 
        r = requests.post('http://0.0.0.0:8000/oauth/token/',
            data = {
                'grant_type': 'refresh_token',
                'refresh_token': request.data['refresh_token'],
                'client_id': CLIENT_ID,
                'client_secret': CLIENT_SECRET
            }
        )

        return Response(r.json())

    @action(detail=False, methods=['post'])
    def revoke_token(self, request):
    
        r = requests.post('http://0.0.0.0:8000/oauth/revoke-token/', 
            data={
                'token': request.data['token'],
                'client_id': CLIENT_ID,
                'client_secret': CLIENT_SECRET,
            },
        )
        
        if r.status_code == requests.codes.ok:
            return Response({'message': 'token revoked'}, r.status_code)
    
        return Response(r.json(), r.status_code)