
# Django
from django.contrib.auth import password_validation
from django.core.validators import RegexValidator

# Models
from core.users.models import User

# DRF
from rest_framework import serializers
from rest_framework.validators import UniqueValidator



class UserModelSerializer(serializers.ModelSerializer):

    class Meta:
        model=User
        fields = (
            'id',
            'username', 
            'email',
            'first_name',
            'last_name',
            'phone_number'
            )
        read_only_fields= ['username', 'email', 'id']

#class NewPasswordSerializer(serializers.Serializer):


class UserSignUpSerializer(serializers.Serializer):

    email = serializers.EmailField(
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    username = serializers.CharField(
        min_length=4,
        max_length=20,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    phone_regex = RegexValidator(
        regex=r'\+?1?\d{9,15}$',
		message="Phone number must be entered in the format: +9999999. Up to 15 digits allowed."
    )

    phone_number = serializers.CharField(validators=[phone_regex])

    # Password	
    password = serializers.CharField(min_length=8, max_length=64)
    password_confirmation = serializers.CharField(min_length=8, max_length=64)

	# Name
    first_name = serializers.CharField(min_length=2, max_length=30)
    last_name = serializers.CharField(min_length=2, max_length=30)

    def validate(self, data):
        passwd = data['password']
        passwd_conf = data['password_confirmation']
        if passwd != passwd_conf:
            raise serializers.ValidationError("Password don't match")
        password_validation.validate_password(passwd)
        return data
    
    def create(self, data):
        """Handle user and profile creation."""
        data.pop('password_confirmation')
        user = User.objects.create_user(**data)        
        return user